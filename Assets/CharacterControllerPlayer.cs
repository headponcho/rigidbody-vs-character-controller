﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterControllerPlayer : MonoBehaviour {

    [SerializeField] float speed = 3.0f;
    [SerializeField] float jumpSpeed = 5.0f;
    [SerializeField] float gravity = 9.8f;

    CharacterController controller;
    Vector3 moveDirection = Vector3.zero;
    bool isGrounded = false;
    float distanceToGround;

	// Use this for initialization
	void Start () {
        controller = GetComponent<CharacterController>();
        distanceToGround = controller.bounds.extents.y + 0.1f;
	}
	
	// Update is called once per frame
	void Update () {
        isGrounded = Physics.Raycast(transform.position, Vector3.down, distanceToGround);
        if (isGrounded) {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

            // Rotate the player the correct way
            if (moveDirection != Vector3.zero) {
                transform.forward = moveDirection;
            }

            moveDirection *= speed;
            if (Input.GetButtonDown("Jump")) {
                moveDirection.y = jumpSpeed;
            }
        } else {
            moveDirection.y -= gravity * Time.deltaTime;
        }

        controller.Move(moveDirection * Time.deltaTime);
    }
}
