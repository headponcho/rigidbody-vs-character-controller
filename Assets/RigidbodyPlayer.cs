﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigidbodyPlayer : MonoBehaviour {

    [SerializeField] float speed = 3.0f;
    [SerializeField] float jumpSpeed = 5.0f;

    Rigidbody rbody;
    Vector3 inputs;
    bool isGrounded = false;
    float distanceToGround;

	// Use this for initialization
	void Start () {
        rbody = GetComponent<Rigidbody>();
        distanceToGround = GetComponent<CapsuleCollider>().bounds.extents.y + 0.1f;
	}
	
	// Update is called once per frame
	void Update () {
        isGrounded = Physics.Raycast(transform.position, Vector3.down, distanceToGround);
        if (isGrounded) {
            inputs = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

            // Rotate the player the correct way
            if (inputs != Vector3.zero) {
                transform.forward = inputs;
            }

            if (Input.GetButtonDown("Jump")) {
                rbody.AddForce(Vector3.up * jumpSpeed, ForceMode.VelocityChange);
            }
        }
    }

    void FixedUpdate() {
        rbody.MovePosition(rbody.position + inputs * speed * Time.fixedDeltaTime);
    }
}
