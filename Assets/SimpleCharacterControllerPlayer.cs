﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleCharacterControllerPlayer : MonoBehaviour {

    [SerializeField] float speed = 500.0f;
    [SerializeField] float jumpSpeed = 2.0f;

    CharacterController controller;
    bool isGrounded = false;
    float distanceToGround;

	// Use this for initialization
	void Start () {
        controller = GetComponent<CharacterController>();
        distanceToGround = controller.bounds.extents.y + 0.6f;
	}
	
	// Update is called once per frame
	void Update () {
        isGrounded = Physics.Raycast(transform.position, Vector3.down, distanceToGround);

        Vector3 velocity = new Vector3();

        velocity.x = Input.GetAxis("Horizontal") * speed;
        velocity.z = Input.GetAxis("Vertical") * speed;
        if (velocity != Vector3.zero) {
            transform.forward = velocity;
        }

        // Jumping will not work since SimpleMove ignores y velocity
        if (Input.GetButtonDown("Jump") && isGrounded) {
            velocity.y = jumpSpeed;
        }

        controller.SimpleMove(velocity * Time.deltaTime);
    }
}
